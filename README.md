# Docker build image nginx with lua modules.
## Preview
This is a pet project. The goal of the project is to learn how to create images from source files.
Here is an assembly of the nginx image with lua modules.
For normal operation, all the necessary dependencies for nginx assembly are fixed by certain versions.
List of dependencies [nginx-1.21.4](https://hg.nginx.org/nginx/archive/):<br>

* [luajit2 v2.1](https://github.com/openresty/luajit2/)<br>
* [lua-resty-core 0.1.27](https://github.com/openresty/lua-resty-core/)<br>
* [lua-resty-lrucache v0.13](https://github.com/openresty/lua-resty-lrucache/)<br>
* [ngx_devel_kit](https://github.com/vision5/ngx_devel_kit/)<br>
* [lua-nginx-module](https://github.com/openresty/lua-nginx-module/)<br>
* [Link for main project.](https://github.com/openresty/lua-nginx-module)<br>

For the project to work in nginx.conf, the following lines must be present in the http directive:
```
lua_package_path "/opt/nginx/lib/lua/?.lua;;";
```

The valid nginx config is located in:

```
 ./{root_project}/nginx/nginx.conf
```

## Run project

### Docker build
To build a container, use the following command in the root of the project:
```
sudo docker build -t {DOCKER_IMAGE_NAME:TAG} .
```
### Docker start
to start the container, use the following command:
```
sudo docker run -v /{PATH_TO_nginx.conf}/nginx.conf:/opt/nginx/conf/nginx.conf -d -p {PORT}:80 --name={NAME_DOCKER}  {DOCKER_IMAGE}
```
