FROM debian:9 as build-luajit
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list
RUN apt update && apt install -y wget gcc make
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20231006.tar.gz && tar xvfs v2.1-20231006.tar.gz
WORKDIR /luajit2-2.1-20231006
RUN make && make install PREFIX=/luajit2/

FROM debian:9 as build-lua-resty-core
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list
RUN apt update && apt install -y wget gcc make
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.27.tar.gz && tar xvfs v0.1.27.tar.gz
WORKDIR /lua-resty-core-0.1.27
RUN make && make install PREFIX=/opt/nginx/
CMD ["bash"]

FROM debian:9 as lua-resty-lrucache
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list
RUN apt update && apt install -y wget gcc make
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.13.tar.gz && tar xvfs v0.13.tar.gz
WORKDIR /lua-resty-lrucache-0.13
RUN make && make install PREFIX=/opt/nginx/


FROM debian:9 as build_nginx
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list
RUN apt update && apt install -y wget gcc make
RUN apt install -y libpcre3 libpcre3-dev zlib1g-dev
RUN wget https://hg.nginx.org/nginx/archive/release-1.21.4.tar.gz && tar xvfz release-1.21.4.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.2.tar.gz && tar xvfz v0.3.2.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.25.tar.gz && tar xvfz v0.10.25.tar.gz
WORKDIR /nginx-release-1.21.4/

COPY --from=build-luajit /luajit2/ /usr/local/
COPY --from=build-lua-resty-core /opt/nginx/ /opt/nginx/
COPY --from=lua-resty-lrucache /opt/nginx/ /opt/nginx/

RUN export LUAJIT_LIB=/usr/local/ && export LUAJIT_INC=/usr/local/include/luajit-2.1 && ./auto/configure --prefix=/opt/nginx \
--with-ld-opt="-Wl,-rpath,/usr/local/lib" \
--add-module=/ngx_devel_kit-0.3.2 \
--add-module=/lua-nginx-module-0.10.25
RUN make -j2
RUN make install
CMD ["/opt/nginx/sbin/nginx","-g","daemon off;"]